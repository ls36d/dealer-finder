import Vue from 'vue';
import Main from './app/Main.vue';
import * as VueGoogleMaps from 'vue2-google-maps';

import './index.scss';

export default new Vue({
  el: '#root',
  render: h => h(Main)
});

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBUw2jHQahd_PsRUZtLeqH9YtY83ESz4w0'
  }
});

